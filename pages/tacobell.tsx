import Head from 'next/head'
import { useRouter } from 'next/router'

interface YesProps {
  yes?: string
}

interface NoProps {
  no?: string
}

function Yes ({ yes }: YesProps) {
  return (
    <h2>
      <a href='https://www.tacobell.com/food'>
        {yes != null ? <>{yes}</> : <>Yes</>}
      </a>

      <style jsx>{`
        h2 {
          font-size: 4rem;
          margin-left: 15px;
          margin-right: 15px;
          margin-top: 0;
          text-align: center;
        }
      `}</style>
    </h2>
  )
}

function No ({ no }: NoProps) {
  return (
    <h2>
      {no != null ? <>{no}</> : <>No</>}

      <style jsx>{`
        h2 {
          margin-left: 15px;
          margin-right: 15px;
          margin-top: 0;
          text-align: center;
        }
      `}</style>
    </h2>
  )
}

function TacoBell () {
  const router = useRouter()
  const yes = Array.isArray(router.query.yes) ? router.query.yes[0] : router.query.yes
  const no = Array.isArray(router.query.no) ? router.query.no[0] : router.query.no
  const wednesday = (new Date()).getDay() === 3

  if (!router.isReady) {
    return null
  }

  return (
    <div>
      <Head>
        <title>Taco Bell Today?</title>
        <meta property='og:image' content='/static/img/taco-bell-social.png' />
        <meta property='og:image:width' content='1200' />
        <meta property='og:image:height' content='630' />
      </Head>

      <img src='/static/img/taco-bell.png' alt='Taco Bell' />
      <h1 className='question'>Taco Bell Today?</h1>
      {router.isReady
        ? <>{no == null && (wednesday || yes != null) ? <Yes yes={yes} /> : <No no={no} />}</>
        : <No no='...' />}

      <style jsx>{`
        img {
          display: block;
          margin-left: auto;
          margin-right: auto;
        }

        h1 {
          text-align: center;
        }
      `}</style>
    </div>
  )
}

export default TacoBell
