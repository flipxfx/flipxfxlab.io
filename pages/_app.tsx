import React from 'react'
import App from 'next/app'
import Head from 'next/head'
import { Nunito } from '@next/font/google'

const nunito = Nunito({ subsets: ['latin'] })

class CustomApp extends App {
  render () {
    const { Component, pageProps } = this.props

    return (
      <div className={nunito.className}>
        <Head>
          <title>flippidippi</title>
          <link rel='icon' href='/static/img/favicon.ico' sizes='any' />
        </Head>

        <Component {...pageProps} />

        <style jsx global>{`
          :root {
            --color-white: #ffffff;
            --color-blue: #01a2fc;
            --color-yellow: #fdd02a;
            --color-purple: #5329cb;
            --color-pink: #ff3e97;
            --color-dark: #150d32;

            --background-image: url(/static/img/background.jpg);
            --background-color: var(--color-dark);
            --text-color: var(--color-white);
            --title-color: var(--color-pink);
            --link-color: var(--color-yellow);
            --link-hover-color: var(--color-blue);
          }

          html, body, #__next {
            background-color: var(--background-color);
            color: var(--text-color);
            height: 100%;
            margin: 0;
            padding: 0;
          }

          a {
            color: var(--link-color);
            text-decoration: none;
          }

          a:hover {
            color: var(--link-hover-color);
          }
        `}
        </style>

        <style jsx>{`
          div {
            height: 100%;
            margin: 0;
            padding: 0;
          }
        `}
        </style>
      </div>
    )
  }
}

export default CustomApp
