
function Whirly () {
  return (
    <div className='root'>
      <div className='row'>
        <div className='col'>
          <a href="javascript:(function()%7B%20var%20style%20=%20document.createElement('style');%20var%20styleContent%20=%20document.createTextNode('body%20*%20%7B%20-webkit-transition:%20all%201.5s%20ease%20!important;%20-moz-transition:%20all%201.5s%20ease%20!important;%20-o-transition:%20all%201.5s%20ease%20!important;%20transition:%20all%201.5s%20ease%20!important;%20%7D%20body%20*:hover%20%7B%20-webkit-transform:%20rotate(180deg)%20!important;%20-moz-transform:%20rotate(180deg)%20!important;%20-o-transform:%20rotate(180deg)%20!important;%20transform:%20rotate(180deg)%20!important;%20%7D');%20style.appendChild(styleContent%20);%20var%20caput%20=%20document.getElementsByTagName('head');%20caput%5B0%5D.appendChild(style);%20%7D)();">
            Whirly
          </a>
        </div>
        <div className='row'>
          <div className='col'>
            Drag Whirly to bookmarks and use on different sites.
          </div>
        </div>
      </div>

      <style jsx global>{`
        html, body, #__next {
          background-color: #1c1c1c;
        }
      `}
      </style>

      <style jsx>{`
        .root {
          height: 100%;
          font-family: 'Lilita One';
          font-size: 1.5em;
          color: #cacfce;
        }

        .row {
          align-items: center;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          margin: 0;
          min-height: 100%;
          padding: 0;
          width: 100%;
        }

        .col {
          text-align: center;
          max-width: 100%;
        }

        a {
          font-size: 4em;
          color: #89da2c;
        }
      `}
      </style>
    </div>
  )
}

export default Whirly
