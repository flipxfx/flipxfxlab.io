import Head from 'next/head'

function Thr33Zer0 () {
  return (
    <div>
      <Head>
        <title>thr33 zer0</title>
      </Head>

      <p>
        <b>ʎqloɔ</b>: d͘r͉͍̦̱̪̰̫a̗̭̣͢g̙̖̭̫̮͈ ̝̘͚̪̞̬̕b̲͖̭̤͎̫̝͡eh҉̘͉͉̭͇͉̭i̷n҉̣̯d̖̥ <b>ho̶̘̩͚̝̗͔r̢s͏̫̘̻̻̼͇ḛ̣̭͖̥̲</b>
      </p>
      <p>
        <b>ʇʇǝuuǝq</b>: r͕͖͓̬̪̞̹͞e͍̥̟̝p̢̳̤̯̪̰l͍̤̣̩͕̙͉̕a̦͜c̪̣̥̲͓͟e̮̥ ͇̻̩g̷̮̞̬i̤̫̰r̭̖̜l̸͔̻͕̗f̺̻̗̜̻̭̣͢r̹ìe͎̘n͈̗͕͈̼d̮̳͓͞'͔̰s̛̹ ͈<b>f̗͍e̟͔̻̺̻̼e̤̪̦t̶̪̯̼̪̺͍̻</b> ̼̞͙w̛̘̘̞i͙͓͍̫͕t͍̺̘̠h̪̝̟ ͉̦͈͇͓̼ͅh͍̰͟i̸͕̘s̸̥̣̳̻̖̦̦ ̳o̖w͚̣̦͢n̩̩̙̤̣
      </p>
      <p>
        <b>uǝq</b>: s̷̘̟͉u҉͉̩̬͕̥̩f͙̙͎͚f͈̬͙̯͖o̸͈̖̼̪c̼̗̪͎̣͇͈a̩̕t͖͖͓͙e͚̩͙ ̜̜̟i͏̰̹̦͉̗̺̞n̛͍͉̬ ̥͎̦͔͕̬d̢̯̦͉͕̜̱͔r̷̤͓̯̝y̛͔̤̥̰͍e̹̠͔͠r͎ ̡̯<b>l̮̦̣̠͝i̝̗̯͙̞̠͞n͈t͞</b>
      </p>
      <p>
        <b>uɐp</b>: d̡̩͍r̗͓̣̲̝̝͔ó͉w̴̩̥̫ņ̠ i̮̟̹n̶͈ ẁa̼͡r͉̘̬͎͉m̭̺̯̟͕͉̱ ͍͉̤<b>m̢̞̮̲̟̤͍i̗̞͖͜lk̗̖̦̩̩</b>
      </p>
      <p>
        <b>ɹǝʇunɥ</b>: f̸̱̜o̧̩̻̗̭͔r͈͔̮̖̺̀c̵͈̫̳͕͕̳e̺̭̞̦͙ ̰̖̱̫̫<b>s̮͚͉̳͓u̩͕̩̜iͅc͓̩̗͖̠i͎̘̙̠d̲͖̲͈̝̺̦͟e̴̗̺̻̻</b> ̠͈̼͔͚̗͝b͜y̪̠ ̩̥͍͖̭p̻̟̞r͈̬̲̤̲͟e̛̖̱̝̣͙̱̖v̯͙̖̥̩̼͇͞e͝n͓͓̹̬̱͜tḭ̹̲͔n̙̪͖̳ͅg͖͈͇̥̤͙ͅ ̪͕̱̖̦͖͙f̯͡r̨̤̲̭̩ͅo̸̻ͅm̢ ͏̣̩̠p̤͓̝̗͔ḛ͕͟t͎͈͈̬t͖̙̟͝i̧͙͉͉ng̗̟ ̩̳d҉̘̺͓̞̯̳͍o̯̤͍̲͡g͇͟
      </p>

      <style jsx>{`
        p {
          transition: font-size 1s;
        }

        p:hover {
          font-size: 20px;
        }

        b {
          color: red;
          transition: font-size 1s;
          font-size: 20px;
        }

        b:hover {
          font-size: 30px;
        }
      `}
      </style>
    </div>
  )
}

export default Thr33Zer0
