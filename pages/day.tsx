import { useMemo } from 'react'
import Head from 'next/head'

function Sunday () {
  return (
    <div>
      sunday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}

function Monday () {
  return (
    <div>
      monday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}

function Tuesday () {
  return (
    <div>
      tuesday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}
function Wednesday () {
  return (
    <div>
      wednesday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}
function Thursday () {
  return (
    <div>
      thursday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}

function Friday () {
  return (
    <div>
      fuck you 🖕

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}

function Saturday () {
  return (
    <div>
      saturday

      <style jsx>{`
        div {
          color: #fb4934;
        }
      `}
      </style>
    </div>
  )
}

function Day () {
  const name = useMemo(() => (new Date()).toLocaleDateString('en-US', { weekday: 'long' }), [])
  const Day = useMemo(() => {
    const days = [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]

    return days[(new Date()).getDay()]
  }, [])

  return (
    <div className='root'>
      <Head>
        <title>{name}</title>
      </Head>

      <div className='row'>
        <div className='col'>
          <Day />
        </div>
      </div>

      <style jsx global>{`
        html, body, #__next {
          background-color: #1c2021;
        }
      `}
      </style>

      <style jsx>{`
        .root {
          height: 100%;
        }

        .row {
          align-items: center;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          margin: 0;
          min-height: 100%;
          padding: 0;
        }

        .col {
          text-align: center;
          width: auto;
          font-family: 'Lilita One';
          font-size: 50px;
        }
      `}
      </style>
    </div>
  )
}

export default Day
