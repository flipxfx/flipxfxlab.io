# flippidippi.gitlab.io
My personal website

## Development
- Run `npm run dev` to start development server

## Production
- Automatic deployment with GitLab CI upon pushing to master branch
